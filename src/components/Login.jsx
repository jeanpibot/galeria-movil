import React, { useState } from 'react';
import '../css/Login.css';
import { Link, useHistory } from 'react-router-dom';
import { authFirebase } from '../lib/firebase';



const Login = () => {

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');


    const signIn = (e) => {
        e.preventDefault();

        authFirebase
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                history.push('/')
            })
            .catch(error => alert(error.message))

    }

    const register = (e) => {
        e.preventDefault();

        authFirebase
            .createUserWithEmailAndPassword(email, password)
            .then((authFirebase) => {
                if (authFirebase) {
                    history.push('/')
                }
            })
            .catch(error => alert(error.message))
    }

    return (
        <div className="login">
            <Link to="/">
                <picture className="login__logo">
                    <img  src="https://i.ibb.co/pn8rbpC/logo-gallery.png" alt="logo-galeria" border="0" />
                </picture>
            </Link>
            <div className="login__container">
                <h1>Inicio de Sesión</h1>

                <form>
                    <label htmlFor="email">
                        <span>E-mail</span>
                        <input id="email" type="text" value={email} onChange={e => setEmail(e.target.value)} placeholder="E-mail" required autoComplete="email" />
                    </label>

                    <label htmlFor="contraseña">
                        <span>Contraseña</span>
                        <input id="contraseña" type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Contraseña" required />
                    </label>

                    <button onClick={signIn} className="login__signInButton" type="submit">Iniciar</button>

                    <button onClick={register} className="login__createAccount">Crea tu cuenta</button>
                </form>
            </div>
            <div className="login__bubbles">
                <div className="login__bubble"></div>
                <div className="login__bubble"></div>
                <div className="login__bubble"></div>
                <div className="login__bubble"></div>
            </div>
        </div>
    )
}

export default Login
