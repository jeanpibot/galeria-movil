import React from 'react';
import { useStateValue } from '../contexts/StateProvider';
import '../css/CheckoutProduct.css';

const CheckoutProduct = ({ id, title, description, image, price, rating, hideButton }) => {

    const [, dispatch] = useStateValue();

    const formatter = new Intl.NumberFormat("en-CO", {
        style: "currency",
        currency: "COP",
      });

    const removeFromBasket = () => {
        dispatch({
            type: "REMOVE_FROM_BASKET",
            id: id,
        });
    }
    return (
        <div className="checkoutProduct">

            <picture className="checkoutProduct__image">
                <img  src={image} alt={title} />
            </picture>

            <div className="checkoutProduct__info">
                <p className="checkoutProduct__tittle">{title}</p>

                <p className="checkoutProduct__description">{description}</p>

                <p className="checkoutProduct__price">
                    <strong>{formatter.format(price)}</strong>
                </p>

                <div className="checkoutProduct__rating">
                    {Array(rating)
                        .fill()
                        .map((_, i) => (
                            <span role="img" aria-label="star" aria-labelledby="star">⭐</span>
                        ))}
                </div>
                {!hideButton && (
                    <button onClick={removeFromBasket}>Remover item</button>
                )}
            </div>
        </div>
    )
}

export default CheckoutProduct
