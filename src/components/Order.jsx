import React from 'react';
import '../css/Order.css';
import CheckoutProduct from './CheckoutProduct';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';

const Order = ({ order }) => {

    return (
        <div className="order">
            <h2>Pedido</h2>
            <time datatime={order.data.created} >
                {moment.unix(order.data.created).format("MMMM Do YYYY, h:mma")}
            </time>
            <p className="order__id">
                <small>{order.id}</small>
            </p>

            {order.data.basket?.map(item => (
                <CheckoutProduct
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    description={item.description}
                    image={item.image}
                    price={item.price}
                    rating={item.rating}
                    hideButton
                />
            ))}


            <CurrencyFormat
                renderText={(value) => (
                    <h3 className="order__total"> Order Total: <strong>{`${value}`}</strong></h3>
                )}

                decimalScale={2}
                value={order.data.amount / 100}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"$"}
            />
        </div>
    )
}

export default Order
