import React, { useState } from 'react';
import '../css/DashBoard.css';
import Product from './Product';
import { toast } from 'react-toastify';


const DashBoard = ({ filteredUsers, currentId, addProduct }) => {


    const initialStateValues = {
        title: "",
        description: "",
        url: "",
        rating: 0,
        price: 0,
    };

    const [values, setValues] = useState(() => initialStateValues);


    const validURL = (str) => {
        var pattern = new RegExp(
          "^(https?:\\/\\/)?" + // protocol
          "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
          "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
          "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
          "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$",
            "i"
        ); // fragment locator
        return !!pattern.test(str);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({ ...values, [name]: value, })
    }


    const handleSubmit = (e) => {
        e.preventDefault();

        if (!validURL(values.url)) {
            return toast("invalid url", { type: "warning", autoClose: 1000 });
        }
        addProduct(values);
        setValues({ ...initialStateValues })

    }


    return (
        <>
            <div className="dashboard">
                <h1>DashBoard de administración</h1>
                <form className="dashboard__form" onSubmit={handleSubmit}>

                    <label htmlFor="producto">
                        <span>Nombre del producto</span>
                        <input id="producto" type="text" value={values.title} name="producto" onChange={handleInputChange} />
                    </label>

                    <label htmlFor="description">
                        <span>Descripción del producto</span>
                        <textarea
                            id="description"
                            type="text"
                            value={values.description}
                            rows="7"
                            cols="50"
                            name="description"
                            onChange={handleInputChange} />
                    </label>

                    <label htmlFor="url">
                        <span>Url de la Imágen</span>
                        <input
                            id="url"
                            type="text"
                            className="form-control"
                            placeholder="https://someurl.xyz"
                            value={values.url}
                            name="url"
                            onChange={handleInputChange}
                        />
                    </label>

                    <label htmlFor="rating">
                        <span>Rating del producto</span>
                        <input id="rating" type="number" value={values.rating} name="rating" min="1" max="5" onChange={handleInputChange} />
                    </label>

                    <label htmlFor="precio">
                        <span>Precio del producto</span>
                        <input type="number" value={values.price} name="price" onChange={handleInputChange} />
                    </label>

                    <button>{currentId === "" ? "Guardar producto" : "Actualizar producto"}</button>
                </form>
            </div>
            <div className="home__row">
                {filteredUsers.map(product => (
                    <Product
                        key={product.id}
                        id={product.id}
                        title={product.title}
                        description={product.description}
                        price={parseInt(product.price)}
                        rating={parseInt(product.rating)}
                        image={product.url}
                    />
                ))}
            </div>
        </>
    )
};

export default DashBoard;